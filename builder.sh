source $stdenv/setup

mkdir -p $out
cd $out
west init -m https://github.com/nrfconnect/sdk-nrf --mr $version
west update

# Remove files that contain timestamps or otherwise have non-deterministic properties.
# Taken from https://github.com/NixOS/nixpkgs/blob/master/pkgs/build-support/fetchgit/nix-prefetch-git.
find . -name .git -type d -prune -print0 | while read -d $'\0' dot_git; do
  echo "Cleaning up $dot_git"
  # rm -rf $dot_git/logs/ $dot_git/hooks/ $dot_git/index $dot_git/FETCH_HEAD $dot_git/ORIG_HEAD \
  #   $dot_git/refs/remotes/origin/HEAD $dot_git/config
  rm -rf $dot_git
done
