{ stdenvNoCC, cacert, openssl, git, python3, version, outputHash }:

let
  pywest = python3.withPackages (ps: [ ps.west ]);
in stdenvNoCC.mkDerivation {
  pname = "sdk-nrf";
  inherit version outputHash;

  nativeBuildInputs = [ cacert openssl git pywest ];
  GIT_SSL_CAINFO = "${cacert}/etc/ssl/certs/ca-bundle.crt";

  builder = ./builder.sh;

  outputHashMode = "recursive";
  outputHashAlgo = "sha256";
}