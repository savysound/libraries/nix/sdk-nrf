{
  description = "Nordic Semiconductors sdk-nrf package";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = (import nixpkgs { inherit system; }).pkgs;
        version = "v2.6.1";
        outputHash = "sha256-oCIMThJ3SY9Y2NATufhhdPD7b0i7LQFD5nm8QExO1XQ=";
      in {
        packages.default = pkgs.callPackage ./. { inherit version outputHash; };
      }
    );
}
